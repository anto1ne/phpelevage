# MySQL dump 8.13
#
# Host: localhost    Database: test
#--------------------------------------------------------
# Server version	3.23.36-log

#
# Table structure for table 'identification'
#

DROP TABLE IF EXISTS identification;
CREATE TABLE identification (
  id smallint(5) unsigned NOT NULL default '0',
  entrée date default NULL,
  tip_tag varchar(4) default NULL,
  cause_entrée enum('naissance','achat') NOT NULL default 'naissance',
  an_id smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (an_id),
  KEY id (id)
) ENGINE=MyISAM;

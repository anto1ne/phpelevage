# MySQL dump 8.13
#
# Host: localhost    Database: test
#--------------------------------------------------------
# Server version	3.23.36-log

#
# Table structure for table 'mises_bas'
#

DROP TABLE IF EXISTS mises_bas;
CREATE TABLE mises_bas (
  id smallint(5) unsigned NOT NULL auto_increment,
  date_mb date default NULL,
  nb_chevreaux tinyint(3) unsigned NOT NULL default '1',
  obs varchar(15) default NULL,
  an_id smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (id),
  KEY an_id (an_id)
) ENGINE=MyISAM;

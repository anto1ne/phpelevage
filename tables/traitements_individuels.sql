# MySQL dump 8.13
#
# Host: localhost    Database: test
#--------------------------------------------------------
# Server version	3.23.36-log

#
# Table structure for table 'traitements_individuels'
#

DROP TABLE IF EXISTS traitements_individuels;
CREATE TABLE traitements_individuels (
  traitement varchar(35) default NULL,
  dose varchar(20) default NULL,
  cause varchar(25) default NULL,
  date_tr date NOT NULL default '2001-01-01',
  an_id smallint(5) unsigned NOT NULL default '0',
  KEY an_id (an_id)
) ENGINE=MyISAM;

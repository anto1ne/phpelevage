# MySQL dump 8.13
#
# Host: localhost    Database: test
#--------------------------------------------------------
# Server version	3.23.36-log

#
# Table structure for table 'individus'
#

DROP TABLE IF EXISTS individus;
CREATE TABLE individus (
  tatouage varchar(5) default NULL,
  boucle varchar(5) default NULL,
  cornes char(1) default 'y',
  sexe enum('f','m') NOT NULL default 'f',
  poil varchar(4) default NULL,
  gras varchar(4) default NULL,
  persistance varchar(4) default NULL,
  caractère varchar(15) default NULL,
  sortie date default NULL,
  cause_sortie enum('mort','réforme') default NULL,
  naisseur varchar(8) NOT NULL default '81121044',
  an_id smallint(5) unsigned NOT NULL auto_increment,
  PRIMARY KEY  (an_id),
  KEY tatouage (tatouage,boucle)
) ENGINE=MyISAM;

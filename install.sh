#! /bin/bash

# phpelevage 0.1
# Copyright (C) 2001 Jean-Michel OLTRA, jm.oltra@libertysurf.fr
# 
# Ce logiciel est un logiciel libre. Vous pouvez le distribuer et/ou le
# modifier en respectant les spécifications de la GNU General Public License 
# ainsi qu'elle est publiée par la Free Software Foundation; version 2 de la 
# licence ou version postérieure (à votre convenance).
# 
# Ce progamme est distribué librement avec l'objectif qu'il puisse être utile,
# mais SANS AUCUNE GARANTIE; sans même une garantie MARCHANDE ni QU'IL PUISSE
# ETRE UTILISABLE POUR QUELQUE USAGE QUE CE SOIT. Voyez la GNU General Public 
# License pour plus de détails.
# 
# Vous devriez avoir une copie de la GNU General Public License incluse
# dans ce logiciel; Si ce n'est pas le cas, écrivez à la Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# Toute redistribution de ce logiciel doit inclure une copie de la GNU General 
# Public License ainsi qu'une copie des précédentes notifications.

# vous utilsez ce script à vos risques et périls... 
# usage: install.sh [base |test | effacer |supprimer]

# répertoire supérieur de l'application
#DOC_ROOT="phpelevage"
DOC_ROOT="phpelevage"
#DOC_ROOT=""

# nom de la base de donnée
#DATABASENAME="phpelevage"
DATABASENAME="phpelevage"

# le répertoire "locahost" du serveur Apache. Voir la définition dans httpd.conf
#DOCUMENTROOT="/usr/local/apache/htdocs"
DOCUMENTROOT="/home/antoine/public_html"
#DOCUMENTROOT=""

# répertoire des binaires de mysql
#MYSQL_BIN="/usr/local/mysq/bin"
MYSQL_BIN="/usr/bin"
#MYSQL_BIN=""

# mot de passe de l'administrateur (root) de mysql 
#MYSQLROOT_PASSWORD="secret"
MYSQLROOT_PASSWORD="secret"

# nom de l'utilisateur sous lequel tourne le serveur (nobody ?)
#APACHEUSER="nobody"
APACHEUSER="antoine"


PS3="tapez votre réponse: 1 or 2-> "

echo ""
echo "j'ose espérer que vous avez lu le manuel..."
echo "avez vous défini les 6 variables ?"

select choix in "oui" "non"; do 
	if [ -z "${choix}" ]; then
		echo "erreur: vouz devez faire un choix." 1>&2
	elif [ "$REPLY" -eq 2 ]; then
		echo ""
		echo "il faut paramétrer l'installation"
		echo "lisez le manuel"
		exit
	else
		echo "super!"
		break
	fi
	echo
done


case $1 in 
	test )
	echo "on crée la base de donnée '${DATABASENAME}'....."
	${MYSQL_BIN}/mysqladmin -u antoine -p${MYSQLROOT_PASSWORD} create ${DATABASENAME}
	for sql in `ls ./tables | grep sql`; do
		${MYSQL_BIN}/mysql -u antoine -p${MYSQLROOT_PASSWORD} ${DATABASENAME} < ./tables/${sql}
	done
	for txt in `ls ./tables | grep txt`; do
		#${MYSQL_BIN}/mysqlimport  -u antoine -p${MYSQLROOT_PASSWORD}  --fields-terminated-by=";" ${DATABASENAME}  ./tables/${txt} 
		${MYSQL_BIN}/mysqlimport  -u antoine -p${MYSQLROOT_PASSWORD} --local --fields-terminated-by=";" ${DATABASENAME}  ./tables/${txt}  --default-character-set=utf8
		#mysql   -u antoine -p${MYSQLROOT_PASSWORD} --execute="LOAD DATA LOCAL INFILE ./tables/${txt} INTO TABLE ${DATABASENAME}.${txt:0:-4}   CHARACTER SET UTF8 FIELDS TERMINATED BY ';'; SHOW WARNINGS"
	done
	echo "les tables sont installées et chargées....."
	;;
	
	effacer )
	echo "on détruit la base, et on la recrée....."
	${MYSQL_BIN}/mysqladmin -u antoine -p${MYSQLROOT_PASSWORD} drop ${DATABASENAME}
	${MYSQL_BIN}/mysqladmin -u antoine -p${MYSQLROOT_PASSWORD} create ${DATABASENAME}
	for sql in `ls ./tables | grep sql`; do
		${MYSQL_BIN}/mysql -u antoine -p${MYSQLROOT_PASSWORD} ${DATABASENAME} < ./tables/${sql}
	done
	exit
	;;

	supprimer )
	echo "on enlève tout, dommage..."
	rm -fR ${DOCUMENTROOT}/${DOC_ROOT}
	${MYSQL_BIN}/mysqladmin -u antoine -p${MYSQLROOT_PASSWORD} drop ${DATABASENAME}
	exit
	;;
	
	base )
	echo "on crée la base de donnée '${DATABASENAME}'....."
	${MYSQL_BIN}/mysqladmin -u antoine -p${MYSQLROOT_PASSWORD} create ${DATABASENAME}
	for sql in `ls ./tables | grep sql`; do
		${MYSQL_BIN}/mysql -u antoine -p${MYSQLROOT_PASSWORD} ${DATABASENAME} < ./tables/${sql}
	done
	echo "les tables sont installées....."
	;;

	* )
	echo "usage: install.sh argument"
	echo "voici la liste des arguments disponibles"
	echo "base:      crée la base vide"
	echo "test:      crée la base avec mes données pour testage"
	echo "effacer:   supprime la base de test et en recrée une vide"
	echo "supprimer: enlève tout, base et répertoires de scripts"
	exit
esac


echo "on crée le répertoire '${DOC_ROOT}' et ses sous-répertoires sur le serveur Apache....."
umask 027
mkdir -p ${DOCUMENTROOT}/${DOC_ROOT}/{php,include,images}

echo "on copie les fichiers....."
cp -f accueil.php ${DOCUMENTROOT}/${DOC_ROOT}/
cp -f ./php/* ${DOCUMENTROOT}/${DOC_ROOT}/php/
cp -f ./include/* ${DOCUMENTROOT}/${DOC_ROOT}/include/
cp -f ./images/* ${DOCUMENTROOT}/${DOC_ROOT}/images/

echo "on attribue les permissions sur le serveur....."
chown -R ${APACHEUSER} ${DOCUMENTROOT}/${DOC_ROOT}
chmod 750 ${DOCUMENTROOT}/${DOC_ROOT}/{php,include,images}
chmod 750 ${DOCUMENTROOT}/${DOC_ROOT}/php/*
chmod 750 ${DOCUMENTROOT}/${DOC_ROOT}/include/*
chmod 750 ${DOCUMENTROOT}/${DOC_ROOT}/images/*
chmod -R 777 ${DOCUMENTROOT} # pas très propre pour l'instant


echo  "voyez le manuel pour les paramétrages post-installation." 
echo  -e "\tservez vous en, j'espère que cela vous sera utile."

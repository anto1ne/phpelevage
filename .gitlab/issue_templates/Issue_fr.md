#### Description

[Description du problème]

#### Étape pour le reproduire

1. [Première Étape]
2. [Deuxième Étape]
3. [et ainsi de suite...]

#### Comportement attendu:

[Description du résultat attendu]

#### Commportement actuel:

[Decription du comportement actuel]

#### Reproductibilité:

[À quelle fréquence cela se reproduit ?]

#### Versions

[Quelle version de PhpElevage êtes-vous en train d'utiliser, et les versions de tous les composant et applications pertinentes, en incluant votre système d'exploitation]

#### Information Complémentaires

[Mentionner toutes informations complémentaires, configuration ou données qui peuvent être nécessaire pour reproduire le problème.]

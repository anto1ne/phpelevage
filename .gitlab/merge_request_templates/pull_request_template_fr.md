## Problème
- *Raison de cette PR*

## Solution
- *Et comment vous l'avez résolu*

## État de la PR
- [ ] Code terminé.
- [ ] Correction ou amélioration testées.
- [ ] Mise à jour depuis la dernière version testée.
- [ ] Peut être revue et testée.

## Validation
---
- [ ] **Revue du code** : 
- [ ] **Accord (LGTM)** :  
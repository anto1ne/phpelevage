# PhpElevage

# <a name="preambule">Préambule</a>

Voici la documentation de PhpElevage 0.1.

Vous adresserez vos remarques à :jm.oltra@club-internet.fr

Ce logiciel est un logiciel libre. Vous pouvez le distribuer et/ou le modifier en respectant les spécifications de la GNU General Public License ainsi qu'elle est publiée par la Free Software Foundation ; version 2 de la licence ou version postérieure, à votre convenance.

Ce programme est distribué librement avec l'objectif qu'il puisse être utile, mais _sans aucune garantie_ ; sans même une _garantie marchande_ ni _qu'il puisse être utilisable pour quelque usage que ce soit_. Voyez la GNU General Public License pour plus de détails.

Vous devriez avoir une copie de la GNU General Public License incluse dans ce logiciel ; Si ce n'est pas le cas, écrivez à la Free Software Foundation, inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.

Toute redistribution de ce logiciel doit inclure une copie de la GNU General Public License ainsi qu'une copie des précédentes notifications.

## <a name="avertissement">avertissement :</a>

Je suis éleveur de chèvres, pas programmeur. Par conséquent le programmeur expérimenté me pardonnera mes lacunes ainsi que les lourdeurs de programmation mais, et je l'en remercie d'avance, pourra me faire part de ses observations sur les scripts.

# <a name="aquoicasert">1. À quoi ça sert ?</a>

## <a name="description">1.1 Description</a>

PhpElevage est une petite application écrite en php (version 4.0.5) qui utilise MySQL (version 3.23.36) avec un interfaçage en html servi par le serveur Apache (version 1.3.19).

Les formulaires d'insertion de données et de requêtes permettent le suivi d'un troupeau (de chèvres chez moi) :

*   enregistrement et suivi des mises bas.
*   enregistrement et suivi des filiations.
*   enregistrement et suivi des soins aux animaux.
*   attribution de " notes " aux animaux pour garder en mémoires certaines caractéristiques individuelles.
*   enregistrement des mouvements d'animaux.
*   suivi des effectifs.

On affiche un formulaire en cliquant sur le lien qui l'annonce dans la page d'accueil qui, au terme de l'installation, va se trouver dans "http://localhost/accueil.php". Personnellement j'en ai fait la page d'accueil de mon navigateur. Il suffit alors de saisir les données dans les champs de formulaires et de cliquer sur le bouton "insérer" ou "rechercher". Le bouton "annuler" annule la saisie. On peut se servir de l'application sans connaître le SQL et l'utilisation de MySQL mais il peut être utile de s'y former pour pouvoir intervenir dans les tables, en cas d'erreur de saisie notamment.

## <a name="limites">1.2 Limites</a>

Pour l'instant il n'a pas été créé de champs gérant les index des animaux en Contrôle Laitier. Seules des notes qualifient les animaux.

Il n'est pas prévu dans la version actuelle de gestion des animaux de boucherie : je n'enregistre pas les naissances d'animaux de boucherie sauf sous forme de nombre indistinct dans la table "mises_bas". Par conséquent on ne peut pas dans cette version suivre des mouvements d'animaux de boucherie (personnellement je gère mes ventes avec une autre application liée à une autre base).

# <a name="Installation">2. Installation</a>

Avant toutes choses, décompressez l'archive dans le répertoire de votre choix, par la commande :

    tar -xpvzf phpelevage.tar.gz -C /chemin/vers/le/répertoire_de_mon_choix

ou en le copiant dans ledit répertoire et en le décompressant ensuite.

## <a name="preinstallation">2.1 Paramétrage pré-installation</a>

### <a name="script_install">2.1.1 1er cas vous avez confiance en mon script d'installation...</a>

Le script "install.sh" écrit en Bash peut créer la base, les tables, les répertoires dans le serveur. Mais pour cela il faut lui donner quelques indications. Il doit s'utiliser sous le compte root.

Editez le fichier "install.sh". Paramétrez les variables :

*   [DOC_ROOT] est le répertoire supérieur de l'application. On y trouve la page d'accueil "accueil.php" et les sous répertoires "images" et "include" comme dans l'archive.
*   [DATABASENAME] est le nom de la base de donnée. Par défaut elle est fixée à "phpelevage', mais vous pouvez modifier le nom. Dans ce cas vous modifierez une variable dans "variables.php" comme indiqué plus loin.
*   [DOCUMENTROOT] est le nom du répertoire contenant vos documents sur le serveur. C'est l'url http://localhost. C'est l'équivalent de la directive "DocumentRoot" de votre fichier httpd.conf (chez moi /usr/local/apache/htdocs).
*   [MYSQL_BIN] est le nom du répertoire contenant les binaires de MySQL (usr/local/mysql/bin chez moi)
*   [MYSQLROOT_PASSWORD] est le mot de passe de l'administrateur mysql, généralement root (chez moi...non, je ne vous le dis pas !).
*   [APACHEUSER] est le nom sous lequel tourne le serveur Apache. Regardez votre httpd.conf, vous aurez une ligne avec : User=quelque-chose. C'est ça. Ou faites un `ps aux awk /apache/ {print $1}` ça vous donnera une idée. C'est nobody sous la Mandrake (sous la Debian aussi mais les droits sont différents).

### <a name="script_manuel">2.1.2 2ème cas : vous êtes peu convaincus de mes capacités de programmeur en bash...</a>

Voyez plus loin pour une installation manuelle.

## <a name="installation">2.2 Installation</a>

Vous pouvez installer la base seule avec les tables vides, ou bien la base avec mes données dans les tables pour tester l'ensemble.

### <a name="install_script">2.2.1 par le script " install.sh'</a>

Entrez dans le répertoire phpelevage puis appelez le script.

    cd phpelevage
    ./install.sh base

Cette commande (appel du script avec l'argument "base") installe la base vide.

Si vous désirez installer la base avec les données, faites :

    ./install.sh test

Ne faites **pas** cette commande **après** la précédente, **mais à la place**. Ainsi que vous le verrez plus loin vous pourrez recréer la base vierge.

Si vous appelez le script sans argument, une aide vous indiquera les différents arguments à votre disposition ainsi que leur action.

### <a name="install_manuel">2.2.2 manuelle</a>

*   créez le répertoire supérieur de l'application. Copiez y le fichier "accueil.php".
*   créez les sous répertoires "images" et "include". Copiez y les scripts que vous trouvez dans les répertoires équivalents de l'archive. Donnez une permission en écriture dans "images" à l'utilisateur du serveur. Conservez la structure de l'arborescence car la fonction include() utilise les chemins relatifs pour vous éviter de configurer la directive "include_path" dans php.ini.
*   créez la base de donnée. Vous avez les scripts sql et txt (pour le testage) issus de mysqldump dans le répertoire "tables" de l'archive.

### <a name="postinstall">2.2.3 dans tous les cas</a>  

*   vous devrez adapter les permissions et les propriétaires/groupes de vos fichiers php et de votre base. Le script install.sh donne tous les droits à l'utilisateur d'Apache ($APACHEUSER) mais vous pouvez les modifier. Vous devrez donner des droits sur la base à l'utilisateur avec GRANT.
*   vous pourrez sécuriser tout ou partie de l'application par un fichier .htacccess.
*   si vous avez utilisé le script "install.sh" cela signifie que vous y avez entré le mot de passe de l'administrateur MySQL. Donnez ce fichier à user=root, group=root (chown root.root install.sh) avec un "chmod 700" ou ré-éditez ce fichier pour supprimer le mot de passe.

## <a name="desinstallation">2.3 Désinstallation</a>

### <a name="erase_data">2.3.1 des données de test</a>

Rappelez le script "install.sh" avec l'argument "effacer".

    ./install.sh effacer

Les tables sont recréées vierges. Vous devrez revérifier les permissions sur les fichiers de la base.

### <a name="erase_all">2.3.2 de la totalité</a>

gasp ! Ça ne vous a pas convaincu.

    ./install.sh supprimer

fera votre affaire en supprimant la base et les répertoires de scripts.

## <a name="postinstallation">2.4 paramétrage post-installation</a>

### <a name="variables">2.4.1 fichier principal : " variables.php'</a>

Vous devrez y renseigner :

  * **$db_login** : qui est votre nom d'utilisateur mysql
  * **$db_password** : votre mot de passe mysql
  * **$dbt** : le nom de la base de donnée. Par défaut "phpelevage".
  * **$ici** : votre numéro d'élevage qui constitue le numéro par défaut de l'élevage naisseur des animaux. Chez vous par conséquent le plus souvent.
  * **$renlimit** : est l'âge à partir duquel un jeune est assimilé à un adulte pour les effectifs. Par défaut 240 jours, soit 8 mois.
  * **$formcolor** : est la couleur des formulaires
  * **$logo** : c'est l'image dans la page d'accueil.
  * **$logo_accueil** : est l'image au sein des formulaires.


### <a name="verif">2.4.2 fonctions_verifications.php</a>

Ce fichier regroupe les fonctions de vérification des données saisies. Vous y trouverez notamment :

  * **$mod_ttg** : est le modèle pour le tatouage : 5 chiffres. La séquence peut commencer par des zéros.
  * **$mod_bcle** : est le modèle pour la boucle : plusieurs chiffres qui peuvent commencer par des zéros.

Si vous avez des systèmes d'identification différents, il vous faut modifier ces expressions régulières....

### <a name="accueil">2.4.3 la page d'accueil : accueil.php</a>

J'ai volontairement commenté, donc désactivé, la section contenant des fonctions de création graphique issues de la libgd avec le support TrueType, car il n'est pas évident que tout un chacun ait un php compilé avec gd et ttf. J'ai gardé des fonctions de base de php pour afficher le texte dans l'image. Ça devrait fonctionner....

Si ça ne marche pas, il faut tout désactiver : on désactive, en php, un paragraphe en l'encadrant entre /* et */.

### <a name="main">2.4.4 le script main.php</a>

En fait lorsque vous appellerez une page vous verrez une URL du style :

    http://127.0.0.1/phpelevage/php/main.php?form=X

où X est un nombre entier.

Le fait est que le script "main.php" appelle les pages qui sont repérées par une valeur de la variable `form`. Vous trouverez les correspondances entre `form` et le nom du script en suivant le lien "correspondances " dans la page d'accueil.

### <a name="mysql_apache">2.4.5 si ce n'est pas fait : mysql et apache</a>


Vérifiez les accès à la base et aux répertoires de scripts : [2.2.3](#postinstall)

Personnellement mysql démarre sous l'utilisateur mysql, le répertoire de la base est en 700, les fichiers de la base (.MYD, .MYI, .frm) sont en 660, appartenant à l'utilisateur mysql, groupe mysql. L'utilisateur de la base doit avoir des droits d'accès aux données : select, insert, delete, update au minimum.

Toujours personnellement, le serveur démarre sous user=nobody, group=nobody, les répertoires de l'application en 750, les fichiers en 640, propriétaire nobody, group root, les sous répertoires php, include et images sont protégés par un fichier .htaccess. Les scripts doivent donc être accessibles à l'utilisateur du serveur.

# <a name="utilisation">3. S'en servir enfin...</a>

## <a name="fonctionnement">3.1 Comment ça marche, au fait !</a>

Ce sont quatre tables qui gèrent l'ensemble. En bref :

  * **la table individus** : renferme les données relatives aux adultes principalement. Elle est liée aux autres par un champ de clé (an_id).
  * **la table mises_bas** : sert à archiver les données issues de la mise bas. Elle est liée à la table "identification" par un champ de clé "id".
  * **la table identification** : archive les données relatives aux jeunes gardés pour le renouvellement du troupeau. Étant donné que l'arrivée d'un animal dans le troupeau commence par sa naissance (sauf en cas d'achat mais c'est une autre histoire) c'est la table "identification" qui conditionne l'identification originelle d'un animal. **Ce qui signifie qu'un animal qui ne passe pas par cette table ne sera pas repéré pour les effectifs et les filiations par exemple.** Dans le cas d'un achat on génère une entrée dans cette table avec une mise bas fictive.
  * **la table traitements_individuels** : archive les traitements individuels aux animaux.

Tout ceci pour répéter que, si vous pouvez entrer tous les animaux que vous voulez dans "individus", vous devez passer par un cycle mère-fille pour l'identification formelle et totale des animaux.

Vous ne pourrez entrer que les mises-bas des animaux enregistrés dans "individus".

Les scripts intègrent des fonctions de vérification des saisies, qui se situent dans "../include/fontions_verifications.php". Notamment pour la vérification du masque de saisie des numéros de tatouage et de boucle [2.4.2](#verif). Si vous travaillez avec une seule identification (tatouage ou boucle) désactivez les fonctions de vérifications ou insérez un identifiant fictif (99999 par exemple) pour tous les animaux pour le champ concerné.

## <a name="load_data">3.2 Charger les données dans les tables</a>

Vous avez plusieurs possibilités pour charger les données dont :

*   vous enregistrez tout en "individus" et vous attendez les prochaines mises-bas pour commencer à entrer les nouveaux animaux nés. Ça ira plus vite mais vous ne pourrez utiliser les possibilités de l'application avant plusieurs années.
*   vous vous munissez de vos archives et de patience (c'est ce que j'ai fait !).
*   vous n'avez pas d'archive ni aucun enregistrement : c'est le moment de commencer...

Comme je vous l'ai précédemment expliqué c'est l'identification par la naissance d'un animal qui fait son entrée dans la base. Il est donc nécessaire de commencer à enregistrer **les animaux les plus vieux** (qu'ils soient présents ou non le jour de la saisie). C'est logique puisqu'un animal doit avoir été enregistré pour "pouvoir" mettre bas.

### <a name="save_adults">3.2.1 enregistrer les adultes</a>

Un adulte est, pour moi, un animal identifié par boucle ET tatouage. Sauf pour le script qui gère les effectifs où on définit une valeur d'âge limite.

  * **les animaux qui n'ont pas de mère identifiée** : sont enregistrés par le formulaire "individus" en cliquant sur le lien "fiche individuelle". Vous pourrez renseigner les champs concernant l'animal en question. Si il est né chez vous il n'est pas nécessaire de renseigner "naisseur" comme vous le précise le texte de l'étiquette du champ si vous avez paramétré la variable "$ici".
  * **pour les animaux qui possèdent une mère identifiée** : vous pouvez entrer les données de deux manières par les liens situés dans le menu "chargement" :
     1. Par le formulaire "individus_plus" en cliquant sur le lien "fille et mère". Alors vous renseignez l'animal dans la partie supérieure comme précédemment et vous renseignez la mère dans la partie inférieure.
    
        Notez que par rapport au formulaire "individus" celui-ci possède un champ "cause d'entrée" car on va renseigner sa naissance. La date de mise-bas correspond à la date d'entrée (de naissance) de l'animal.
    
        Dans ce cas là on entre _une_ fille ET _sa_ mère. Si cette mère a eu deux filles de la même mise-bas il sera nécessaire de faire deux saisies (mais le script ne générera qu'une mise-bas).
    
        Répétons le encore une fois, la mère doit avoir été enregistrée précédemment. Sinon vous aurez  "cet animal n'est pas enregistré" !
    
     1. <a name="charger"></a>En rentrant tous les animaux par "fiche individuelle" puis en rentrant toutes les mises-bas avec le lien "avec renouvellement" du menu "mises-bas". Dans ce cas là vous devrez faire les mises à jour sur les notes en cliquant sur "notation adultes" et sortir les animaux avec "sorties adultes". Ce qui fait beaucoup de manipulations.
    


**les achats d'animaux**

<a name="achats"></a>se saisissent avec le groupe de formulaires que l'on obtient en cliquant sur "achats d'animaux". L'objectif est ici de générer une mise-bas fictive avec une mère bidon ! On va utiliser d'abord le formulaire de gauche "achat d'animaux". Si, par exemple, j'achète un animal à l'éleveur Dupont, je vais rentrer dans le champ origine la valeur "dupon" (5 caractères maxi), son numéro d'élevage dans le champ "naisseur", la date d'achat ainsi que le nombre d'animal acheté (1). Le script gère cette saisie comme une mise-bas de l'animal de tatouage "dupon" et lui met une boucle "A". Cet animal rentre dans la table " individus " avec une sortie `NULL`, ce qui fait qu'il n'apparaîtra jamais. Dans le cas où, plus tard, vous rachèteriez un animal à cet éleveur Dupont, le script ira chercher la valeur du champ an_id de la bête fictive "dupon". Puis on identifie l'animal acheté avec le formulaire de droite "identification achat". Si l'animal acheté est adulte on identifie son tatouage et sa boucle. On pourra utiliser ce formulaire autant de fois de suite qu'on aura acheté d'animaux de même origine, c'est en fait un sous formulaire.

### <a name="enregistrement_jeunes">3.2.2 enregistrer les jeunes</a>

<a name="renouvellement"></a>On va enregistrer les jeunes comme une mise-bas ordinaire. On utilise pour ce faire le lien "avec renouvellement" du menu "mises-bas". On va rentrer tout ce qui concerne la mère dans le formulaire de gauche "mise-bas" puis tout ce qui concerne la ou les filles (ou fils) dans le sous formulaire de droite "identification". On pourra saisir autant de fils et filles issus de la mise-bas précédente à la suite en utilisant ce même sous formulaire. Il ne faut pas oublier de les identifier par un **tip_tag** qui est leur identifiant en tant qu'animal jeune. Vous remarquerez que ce sous formulaire prévoit des champs "tatouage" et "boucle" pour identifier des adultes. C'est ce dont je parlais dans une sous-section précédente [3.2.1](#charger)

### <a name="enregistrement_traitements">3.2.3 enregistrer des traitements</a>

Si vous avez gardé traces de traitements aux animaux vous pouvez les saisir dans le formulaire prévu à cet effet en suivant le lien "traitements individuels". Il suffit de renseigner les champs.

## <a name="quotidien">3.3 utiliser quotidiennement</a>

Vous avez différents formulaires qui, au quotidien, permettent de modifier les données des tables ou d'afficher des résultats de requêtes.

### <a name="mises_bas">3.3.1 les mises-bas</a>

Deux formulaires, dans le menu "mises-bas", sont utilisés pour enregistrer les mises-bas, en fonction de la présence, ou non, d'un animal de renouvellement à cette mise-bas.


  * **sans renouvellement** : enregistre une mise-bas simple, il n'y a pas d'entrée dans la table " identification'.
  * **avec renouvellement** : permet d'enregistrer une mise-bas avec un ou plusieurs animaux de renouvellement. Mais on a déjà vu ça [3.2.2](#renouvellement)

### <a name="modifications">3.3.2 pour modifier</a>

On va trouver ces formulaires dans le menu " mises à jour'.

  * **réforme renouvellement** : s'utilise pour sortir des animaux qu'on avait gardés pour le renouvellement du troupeau mais qui n'y sont plus pour une raison ou une autre. Ces animaux sont identifiés par leur **tip_tag** et leur numéro d'élevage pour éviter des confusions.
  * **identification définitive** : est utilisé pour identifier pleinement un animal de renouvellement lors de la pose de la boucle adulte (la grosse !) ou du tatouage auriculaire. Le script repère les animaux qui ont un **tip_tag** mais pas de tatouage ou pas de boucle et donne la possibilité de rentrer ces valeurs.
  * **notation adultes** : permet de modifier ou de définir les notes attribuées aux animaux. Il suffit d'identifier l'animal et d'attribuer la ou les notes dans les champs concernés.
  * **rebouclage** : en cas de perte de boucle, il va falloir reboucler l'animal avec une boucle qui comportera un numéro différent de l'original. Afin de conserver une cohérence dans les données le formulaire de rebouclage met la table "individus" à jour. **Il est nécessaire d'utiliser ce formulaire en cas de rebouclage, ou de mettre à jour avec une instruction sql**.
  * **sortie adultes** : met à jour la table "individus" en indiquant une date de sortie et une cause à l'animal qui ne fait plus partie du troupeau.


### <a name="requetes">3.3.3 pour obtenir des données</a>

On trouvera ces formulaires dans le menu "requêtes".

  * **ascendance et descendance** : permet d'afficher la descendance, l'ascendance ou la fiche technique de l'animal sélectionné. Pour les parentèles, les animaux doivent avoir été identifiés par une mise bas, ou son équivalent avec le lien "fille et mère".
  * **historique des soins** : affiche dans un tableau les soins prodigués à un animal ou sur une période donnée.
  * **historique des mises-bas** : retrace les mises-bas sur une période donnée.
  * **effectifs** : permet de visualiser au choix :
    *   tous les animaux adultes présents au jour de la requête
    *   les effectifs adultes au jour de la requête
    *   la variation d'effectif entre deux dates, incluant les effectifs début, les entrées d'animaux nés sur l'élevage, les entrées d'animaux achetés, les sorties d'animaux et l'effectif en fin de période. Les calculs sont effectués sur les reproducteurs (mâles et femelles) et sur les jeunes (mâles et femelles également).

Dans la version de test les variations d'effectifs ne sont cohérents qu'à partir du 01-01-2000 à cause des données manquantes avant le 1er janvier 2000.

# <a name="todo">4. À faire...</a>

En fonction du "succès" de cette application il serait possible :

*   d'intégrer des saisies spécifiques à certains élevages : élevages laitiers en Contrôle Laitier, élevage d'animaux de boucherie par exemple.
*   permettre les sorties de requêtes en format pdf pour une impression convenable.
*   modifier le mode d'appel des pages (pseudo-frames) si celui-ci n'est pas assez explicite.

* * *

2001-10-28 : [Jean-Michel Oltra](mailto:jm.oltra@club-internet.fr)
2020 : Antoine
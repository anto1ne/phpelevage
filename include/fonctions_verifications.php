<?php

$mod_ttg = "^[0-9]{5}$";
$mod_bcle = "^[0-9]+$";
$mod_dte = "^([0-3][0-9])[^0-9]([0-1][0-9])[^0-9]([0-9]{4})$";
$mod_dblpos = "^[0-9]+\.?[0-9]*$";
$mod_dbl = "^\-?[0-9]+\.?[0-9]*$";
$mod_int = "^[0-9]+$";
$mod_nelv = "^[0-9]{8}$";

function verifValidite($modèle,$lbl,$field)
{
	global $vérif;
	if(!ereg($modèle,$field))
	{
		message("la saisie du champ ". "\"$lbl\"" . " n'est pas valide.");
		print("<br>\n");
		$vérif = 1;
		return 1;
	}
				
}

function verifSaisie($lbl,$field)
{
	global $vérif;
	if(!isset($field))
	{
		message("le champ " . "\"$lbl\"" . " n'est pas saisi.");
		print("	<br>\n");
		$vérif = 1;
		return 2;
	}
}

function convertDate($dte)
{
	global $convdte;
	$jour = substr($dte,0,2);
	$mois = substr($dte,3,2);
	$année = substr($dte,-4);
	$convdte = $année . "-" . $mois . "-" . $jour;
}

function verifLongueur($nbcar, $lbl, $field)
{
	global $vérif;
	$nbcar -= 2;
	if(!ereg("^[[:alnum:]].{0,$nbcar}[[:alnum:]]?$",$field))
	{
		$nbcar += 2;
		message("le champ ". "\"$lbl\"" . " doit comporter " . $nbcar .
			" caractères au plus, <br>
			débutant et finissant par un caractère alphanumérique.");
		print("<br><br><br>\n");
		$vérif = 1;
		return 3;
	}
}
		
function setNaisseur($field, $lbl, $modèle)
{
	global $naisseur;
	global $ici;
		
	if(!$field)
	{
		$naisseur = $ici;
	}
	else
	{
		verifValidite($modèle, $lbl,$field);
	}
}
	
function verifPeriode($dd, $df)
{
	$sep = substr($dd, 2, 1);
	$expl_dd = explode($sep, $dd);
	$expl_df = explode($sep, $df);
	$tmstp_dd = mktime(0, 0, 0, $expl_dd[1], $expl_dd[0], $expl_dd[2]);
	$tmstp_df = mktime(0, 0, 0, $expl_df[1], $expl_df[0], $expl_df[2]);	
	if($tmstp_dd > $tmstp_df)
	{
		message("la date de début de période est supérieure à 
			celle de fin de période");
		print("<br>\n");
		$vérif = 1;
		return 4;
	}
}

		
function existence($ttg, $bcl, $nss)
{
	global $this_an_id;
	global $mysql_link; 
	
	$q = "SELECT an_id FROM individus WHERE ";
	$cc = $ttg . $bcl;
	switch($cc)
	{
		case $ttg:
		$q .= "tatouage = '$ttg' ";
		$q .= "AND boucle != '' ";
		break;

		case $bcl:
		$q .= "boucle = '$bcl' ";
		$q .= "AND tatouage != '' ";
		break;

		default:
		$q .= "tatouage = '$ttg' ";
		$q .= "AND boucle = '$bcl' ";
		break;
	}
	$q .= "AND naisseur = '$nss' ";
	$r = mysql_query($q, $mysql_link);
	$n = mysql_affected_rows($mysql_link);

	if($n == 0)
	{
		message("cet animal n'est pas enregistré.");
		exit();
	}
	elseif($n > 1)
	{
		message("plusieurs animaux correspondent à la sélection<br>
			il faut identifier l'animal par tatouage ET boucle.");
		exit();
	}
	else
	{
		$this_an_id = mysql_result($r, 0, "an_id");
	}
}
				
?>

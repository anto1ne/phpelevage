<?php

include("./include/variables.php");
	

$im = ImageCreate(500,110);
//$blue = imagecolorallocate($im,153,102,204);
$bgcolor = imagecolorallocate($im,113,136,153);
$noir = ImageColorAllocate($im,0,0,0);
$vert = imagecolorallocate($im,51,204,51);
$arc_color = imagecolorallocate($im,113,136,153);
$blanc = ImageColorAllocate($im,255,255,255);
//$rouge = imagecolorallocate($im,255,0,0);
$my_color = imagecolorallocate($im,102,102,204);
imagearc($im,250,55,350,100,0,360,$arc_color);
imagefill($im,250,55,$my_color);

//premier choix d'en-tête utilisant les fonctions de gd

/*
$string = "phpelevage";
$font = "/usr/share/fonts/ttf/western/babelfish.ttf";
$font_height = 50;
$ttfbox = imagettfbbox($font_height,0,$font,$string);
$start_x = 250 - (($ttfbox[0] + $ttfbox[2] + $ttfbox[4] + $ttfbox[6]) / 4);
$start_y = 55 - (($ttfbox[1] + $ttfbox[3] + $ttfbox[5] + $ttfbox[7]) / 4);
imagettftext($im,$font_height,0,$start_x,$start_y,$blanc,$font,$string);
//2ÃẀme ligne de texte
$vert = imagecolorallocate($im,51,204,51);
$str = "L'elevage opensource";
$font_height_str = 25;
$ttfbox_str = imagettfbbox($font_height_str,0,$font,$str);
$start_x = 250 - (($ttfbox_str[0] + $ttfbox_str[2] + $ttfbox_str[4] + $ttfbox_str[6]) / 4);
$start_y = 85 - (($ttfbox_str[1] + $ttfbox_str[3] + $ttfbox_str[5] + $ttfbox_str[7]) / 4);
imagettftext($im,$font_height_str,0,$start_x,$start_y,$vert,$font,$str);
imagepng($im,"./images/phpelevage.png");
imagedestroy($im);
*/


// 2ème choix d'en-tête , fonctions de polices de php

$string = "phpelevage";
$police = 5;
$font_height = ImageFontHeight($police);
$font_width = ImageFontWidth($police);
$start_x = 250 - (strlen($string) * $font_width / 2);
$start_y = 25 + $font_height / 2;
ImageString($im, $police, $start_x, $start_y, $string, $vert);
# 2ème ligne de texte
$string2 = "l'elevage opensource";
$start_x2 = 250 - (strlen($string2) * $font_width / 2);
$start_y2 = $start_y + 20;
ImageString($im, $police, $start_x2, $start_y2, $string2, $vert);
imagepng($im, "./images/phpelevage.png");
imagedestroy($im);

?>

<!DOCTYPE html>
<html lang="fr">
 <head>
  <title>accueil</title>
  <meta charset="UTF-8">
 </head>

 <body bgcolor=778899>
  <center><img src="./images/phpelevage.png" ></center>
  <table border="0" cellpadding="6" width="100%">
   <tr>
    <td bgcolor="#000000" height="45">
<!--cette cellule contient la table qui représente un bandeau noir-->
     <table width="100%" cellspacing="0" cellpadding="0">
      <tr>
       <td width="7%"> 
        <img src="./images/chèvre_50x50.png">
       </td>
       <td  align=left bgcolor="#000000">
        <font color=chartreuse>
		Utiliser les liens pour la gestion du troupeau
		</font>
       </td>
      </tr>
     </table>
    </td>
   </tr> 
	 
   <tr> 
    <td>
  
	    <!--cette cellule contient le reste,c'est à  dire une autre table
	    qui contiendra des tables de dimensions plus petites-->
	   
     <table width="100%" cellspacing="2" cellpadding="3">
	   
	     <!--cette table contient des tables toutes en hauteur dont chaque
	     rang contient une cellule unique, le fond coloré.Ces cellules contiennent 
	     les tables-menus(qui sont elles-même des tables)--> 
	   
      <tr>
       <td>
        <table cellpadding="3">
         <tr>
		   
            <!--voici un fond coloré, 1er rang, 1ère cellule-->
		  <td bgcolor=666699 width=200>
			
		    <!--et là  une table-menu-->   
			 
           <table border=0 width=100% cellspacing=1 cellpadding=4>
            <tr>
             <td align=left bgcolor=9999cc colspan=1>
              <font size=3><b>
              chargement
              </b></font>
	 	     </td>
            </tr>
            <tr>
             <td align=left bgcolor=ccccFF>
	          <li><a href="./php/main.php?form=4">fiche individuelle</a>
			  <li><a href="./php/main.php?form=5">fille et mère</a>
             </td>
            </tr> 
           </table>
		  </td> 
          <td bgcolor=666699 width=200>
           <table border=0 width=100% cellspacing=1 cellpadding=4>
            <tr>
             <td align=left bgcolor=9999cc colspan=1>
              <font size=3><b>
              mises-bas
              </b></font>
	 	     </td>
            </tr>
            <tr>
             <td align=left bgcolor=ccccFF>
              <li><a href="./php/main.php?form=1">sans renouvellement</a>
	          <li><a href="./php/main.php?form=2">avec renouvellement</a>
	          <li><a href="./php/main.php?form=3">identification</a>
             </td>
            </tr> 
           </table>
	      </td>
	      <td bgcolor=666699>
		    <!--fond coloré, 2ème colonne, 1ère et unique cellule-->
	       <table border=0 width=100% cellspacing=1 cellpadding=4>
	        <tr>
	         <td align=left bgcolor=9999cc>
	          <font size=3><b>
	          saisies individuelles
	          </b></font>
	         </td>
	        </tr>
	        <tr>
	         <td align=left bgcolor=ccccFF>
  			  <li><a href="./php/main.php?form=7">achat d'animaux</a>
			  <li><a href="./php/main.php?form=6">traitements individuels</a>
	         </td>
	        </tr>
	       </table> 
          </td>
		   <!--3ème colonne-->
	      <td bgcolor=666699>
	       <table border=0 width=100% cellspacing=1 cellpadding=4>
	        <tr>
	         <td align=left bgcolor=9999cc>
	          <font size=3><b>
	          mises à  jour
	          </b></font>
	         </td>
	        </tr>
	        <tr>
	         <td align=left bgcolor=ccccFF>
	       	  <li><a href="./php/main.php?form=10">réforme renouvellement</a>
			  <li><a href="./php/main.php?form=11">identification définitive</a>
			  <li><a href="./php/main.php?form=12">notation adultes</a>   
			  <li><a href="./php/main.php?form=8">rebouclage</a>
	          <li><a href="./php/main.php?form=9">sorties adultes</a>
	         </td>
	        </tr>
	       </table>	
	      </td>
	      <td bgcolor=666699>
	       <table border=0 width=100% cellspacing=1 cellpadding=4>
	        <tr>
	         <td align=left bgcolor=9999cc>
	          <font size=3><b>
	          requêtes
	          </b></font>
	         </td>
	        </tr>
	        <tr>
	         <td align=left bgcolor=ccccFF>
	          <li><a href="./php/main.php?form=13">ascendance et descendance</a>
			  <li><a href="./php/main.php?form=15">historique des soins</a>
			  <li><a href="./php/main.php?form=16">historique des mises-bas</a> 
	          <li><a href="./php/main.php?form=14">effectifs</a>
		  <li><a href="./php/info.html">correspondances</a>
	         </td>
	        </tr>
	       </table>
          </td>
         </tr>
        </table>	

     </td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td>
   <table width=100%>
    <tr>
     <td align=center>
      <img src="./images/php-med-trans-dark.gif">
     </td> 
	</tr>
   </table>
  <td>
 </tr> 
</table>	
</body>
</html>



<?php

info("utiliser ce formulaire pour sortir des animaux de renouvellement");
print("<br><br>\n");
détail("le bouton 'mettre à jour' insère une date de sortie pour l'animal concerné<br>
		le bouton 'détruire' supprime l'enregistrement, qui n'apparaîtra alors
		plus dans les variations d'effectifs");
print("<br><br>\n");
détail("les champs astérisqués sont obligatoires, sauf la date pour détruire uniquement");
print " <br><br><br><br>
	<form method=\"post\" action=\"main.php?form=10\">
	<table>";

print " <tr>
	<td align=left>
	<label for=\"renouvellement\">
	<font size=5><b>
	renouvellement*
	</b></font></label>
	</td>
	<td align=left>
	<font size=+2>
	<select name=\"renouvellement\">";
		
$mysql_link = mysql_connect($db_server, $db_login,$db_password);
mysql_select_db($dbt, $mysql_link);
$query = "SELECT ii.tip_tag, i.naisseur ";
$query .= "FROM identification ii, individus i ";
$query .= "WHERE (i.tatouage = '' ";
$query .= "OR i.boucle = '') ";
$query .= "AND i.sortie = '0' ";
$query .= "AND ii.an_id = i.an_id ";
$result = mysql_query($query, $mysql_link);
while($row = mysql_fetch_row($result))
{
		$ren = "tip-tag:" . $row[0];
		$ren .= ", naisseur:" . $row[1];
		print " <option>
				$ren
				</option>";
}
print " <option selected> </option>
	</select>
	</td>
	</tr>";
		
print("<tr>");
printRow("date de sortie*<br>
			<font size=3>format:jj-mm-aaaa<br></font>
			<font size=1>ou autre séparateur non numérique</font>",
			"date","sortie","10");
print("</tr>\n");

generateHtmlFoot("annuler", "mettre à jour");

if(empty($renouvellement))
{
		message("il faut identifier l'animal");
		exit();
}
$idf = explode(",", $renouvellement);
$tip_tag = substr($idf[0], 8);
$naisseur = substr($idf[1], 10);

		$q = "SELECT i.an_id ";
		$q .= "FROM individus i, identification ii ";
		$q .= "WHERE ii.tip_tag = '$tip_tag' ";
		$q .= "AND (i.tatouage = '' OR i.boucle = '' ";
		$q .= "AND i.sortie = '0') ";
		$q .= "AND i.naisseur = '$naisseur' ";
		$r = mysql_query($q);
		$an_id = mysql_result($r, 0, "an_id");
		
		
switch($soumettre)
{
		case "détruire":
		
		$query1 = "DELETE FROM identification ";
		$query1 .= "WHERE an_id = '$an_id' ";
		$result1 = mysql_query($query1);
		$maj1 = mysql_affected_rows($mysql_link);
		print " <p class=\"message\">
				$maj1 suppression dans 'identification'
				</p>
				<br>";	
		
		$query2 = "DELETE FROM individus ";
		$query2 .= "WHERE an_id = '$an_id' ";
		$result2 = mysql_query($query2);
		$maj2 = mysql_affected_rows($mysql_link);
		print " <p class=\"message\">
				$maj2 suppression dans 'individus'
				</p>
				<br>";
	
		break;

		case "mettre à jour":
		
if(!verifSaisie("date de sortie", $sortie))
		verifValidite($mod_dte, "date de sortie", $sortie);
if($vérif)
		exit();
		convertDate($sortie);
		$sortie = $convdte;
		$query = "UPDATE individus ";
		$query .= "SET sortie = '$sortie' ";
		$query .= "WHERE an_id = '$an_id' ";

		$result = mysql_query($query, $mysql_link);
		maj($mysql_link);

		break;
}

?>

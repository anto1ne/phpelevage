<?php
include("../include/fonctions_tableaux.php");

$mysql_link = mysql_connect($db_server, $db_login, $db_password);
mysql_select_db($dbt, $mysql_link);
				
		
if(!verifSaisie("date de début de période", $date_début))
		verifValidite($mod_dte, "date de début de période", $date_début);
if(!verifSaisie("date de fin de période", $date_fin))
		verifValidite($mod_dte, "date de fin de période", $date_fin);
if($vérif)
		exit();
if(verifPeriode($date_début, $date_fin))
		exit();
		
convertDate($date_début);
$mydate_début = $convdte;
convertDate($date_fin);
$mydate_fin = $convdte;

$query = "SELECT i.tatouage, i.boucle, CONCAT(right(m.date_mb,2), substring(m.date_mb,5,4), left(m.date_mb,4)) AS \"date de mise-bas\", ";
$query .= "nb_chevreaux AS \"nombre de chevreaux\", obs AS observations ";
$query .= "FROM individus i, mises_bas m ";
$query .= "WHERE m.date_mb BETWEEN '$mydate_début' AND '$mydate_fin' ";
$query .= "AND m.an_id = i.an_id ";
$result  = mysql_query($query, $mysql_link);
$title = "mises-bas du ". $date_début . " au " . $date_fin;
makeColouredTable($title,$result);

generateFormRequest("main.php?form=16");

print "		</body>
		</html>";
?>

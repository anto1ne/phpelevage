<?php

print " <p class=\"info\"> 
	utiliser ce formulaire pour obtenir des infos sur<br>
	l'historique médicale d'un animal ou sur les soins sur une période donnée
	</p>
	<br><br><br>
	<form method=\"post\" action=\"main.php?form=15.1\">
	<font size=\"4\" color=purple>
	sélectionner le type de requête désiré
	</font>
	<br>
	<font size=\"5\">
	<input type=\"radio\" name=\"type_historique\" value=\"animal\">
	historique sur un animal
	<br>
	<input type=\"radio\" name=\"type_historique\" value=\"période\">
	historique sur une période
	<br>
	</font>
	<br><br>
	<font size=\"4\" color=purple>
	dans le cas <u>d'un historique sur un animal donné</u>
	<br>
	identifier l'animal par son tatouage OU sa boucle pour éliminer une cause d'erreur de saisie
	<br>";
		
print("<table>\n");
print("<tr>");
printRow("tatouage","text", "tatouage", 12);
print("</tr>\n");

print("<tr>\n");
printRow("boucle", "text", "boucle",12);
print("</tr>\n");

print("<tr>\n");
printRow("naisseur <br>
	<font size=1>par défaut $ici</font>" , "text", "naisseur", "10");
print("</tr>\n");

print " </table>
	<font size=\"4\" color=purple>
	dans le cas<u> d'une requête sur une période</u><br>
	entrer une date de début ainsi qu'une date de fin de période<br>
	ce n'est valable qu'à partir du 01-01-2001<br>
	</font>
	<br>

	<table>\n";

print("<tr>");
printRow("date de début de période* <br>
	<font size=3>format:jj-mm-aaaa<br></font>
	<font size=1>ou autre séparateur non numérique</font>",
	"date","date_début","10");
print("</tr>\n");

print("<tr>");
printRow("date de fin de période* <br>
	<font size=3>format:jj-mm-aaaa<br></font>
	<font size=1>ou autre séparateur non numérique</font>",
	"date","date_fin","10");
print("</tr>\n");

generateHtmlFoot("annuler", "rechercher");
	
?>

		

<?php
print "	<p class=\"info\"> 
	utiliser ce formulaire pour obtenir des infos 
     	sur l'effectif jour J ou sur des variations d'effectif
	</p>
	<br><br><br><br>
	<form method=\"post\" action=\"main.php?form=14.1\">
	
	<font size=\"4\" color=purple>
	sélectionner le type de requête désiré
	</font>
	<br>
	<font size=\"5\">
	<input type=\"radio\" name=\"type_requête\" value=\"animaux_J\">
	animaux adultes présents jour J
	<br>
	<input type=\"radio\" name=\"type_requête\" value=\"effectif_J\">
	effectif adulte jour J
	<br>
	<input type=\"radio\" name=\"type_requête\" value=\"variation\">
	variation d'effectif
	</font>
	<font size=\"4\" color=purple>
	<br><br>
	<font size=\"4\" color=purple>
	dans le cas d'une requête sur une variation d'effectif<br>
	entrer une date de début ainsi qu'une date de fin de période<br>
	ce n'est valable qu'à partir du 01-01-2000<br>
	</font>
	<br>";

print("<table>");

print("<tr>");
printRow("date de<br>début de période* <br>
	<font size=3>format:jj-mm-aaaa<br></font>
	<font size=1>ou autre séparateur non numérique</font>",
	"date","date_début","10");
print("</tr>\n");

print("<tr>");
printRow("date de<br>fin de période* <br>
	<font size=3>format:jj-mm-aaaa<br></font>
	<font size=1>ou autre séparateur non numérique</font>",
	"date","date_fin","10");
print("</tr>\n");

generateHtmlFoot("annuler", "rechercher");
	
?>

		

<?php
include("../include/fonctions_tableaux.php");

setNaisseur($naisseur, "naisseur", $mod_nelv);
$mysql_link = mysql_connect($db_server, $db_login, $db_password);
mysql_select_db($dbt, $mysql_link);
switch($type_historique)
{
	case "animal":
	if ($date_début || $date_fin)
	{
		message("on ne sélectionne pas de période <br>
			dans l'option \"historique sur un animal\" ");
		break;
	}
	if(!isset($tatouage) && !isset($boucle))
	{
		message("il faut identifier l'animal");
		exit();
	}
	if($tatouage)
		verifValidite($mod_ttg, "tatouage", $tatouage);
	if($boucle)
		verifValidite($mod_bcle, "boucle", $boucle);
	if($vérif)
		exit();
		
	existence($tatouage, $boucle, $naisseur);
		
	$query = "SELECT i.tatouage, i.boucle, ";
	$query .= "CONCAT(right(t.date_tr,2), substring(t.date_tr,5,4), left(t.date_tr,4)) AS \"date de traitement\", ";
	$query .= "t.traitement, t.dose, t.cause ";
	$query .= "FROM individus i,traitements_individuels t ";
	$query .= "WHERE i.an_id = '$this_an_id' ";
	$query .= "AND t.an_id = i.an_id ";
		
	$result = mysql_query($query, $mysql_link);
	$mytatouage = mysql_result($result, 0, "tatouage");
	$myboucle = mysql_result($result, 0, "boucle");
	mysql_data_seek($result, 0);
	$title = "tatouage:".$mytatouage." boucle:".$myboucle;
	makeColouredTable($title,$result);
	break;
		
	case "période":

	if($tatouage || $boucle)
	{
		message("on n'identifie pas d'animal<br>
			dans l'option \"historique par période\" ");
		break;
	}
		
	if($date_début > $date_fin)
	{
		message("la date de début de période est supérieure à celle de fin");
		break;
	} 
		
	if(!verifSaisie("date de début de période", $date_début))
		verifValidite($mod_dte, "date de début de période", $date_début);
	if(!verifSaisie("date de fin de période", $date_fin))
		verifValidite($mod_dte, "date de fin de période", $date_fin);
	if($vérif)
		exit();
	if(verifPeriode($date_début, $date_fin))
		exit();
	convertDate($date_début);
	$mydate_début = $convdte;
	convertDate($date_fin);
	$mydate_fin = $convdte;
	$query = "SELECT CONCAT(right(t.date_tr,2), substring(t.date_tr,5,4), left(t.date_tr,4)) AS \"date de traitement\", ";	
	$query .= "i.tatouage, i.boucle, t.traitement, t.dose, t.cause ";
	$query .= "FROM individus i, traitements_individuels t ";
	$query .= "WHERE t.date_tr BETWEEN '$mydate_début' AND '$mydate_fin' ";
	$query .= "AND t.an_id = i.an_id ";
	$result  = mysql_query($query, $mysql_link);
	$title = "soins du ". $date_début . " au " . $date_fin;
	makeColouredTable($title,$result);
	break;

	default:
	message("il faut sélectionner un type d'historique");
	break;

}
print "	<br><br>
	<form method=\"post\" action=\"historique_soins.php\">
	<table>
	<tr>
	<td>
	<input type=\"submit\" name=\"soumettre\" value=\"nouvelle requête\">
	</td>
	<td align=center>
	<a href=\"../accueil.php\">
	<img src=\"../images/$logo_accueil\">
	</a>
	</td>
	</tr>	
	</table>
	</form>
	</body>
	</html>";
?>

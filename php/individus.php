<?php
info("utiliser ce formulaire pour renseigner des animaux dont on ne connait pas l'ascendance.<br>
si l'ascendance est connue, passer par 'mises_bas' et 'identification'.");
print("<br><br><br>\n");
détail("renseigner poil, gras, persistance avec des + ou des -, 3 au maximum");
print("<br>\n");
détail("les champs astérisqués sont obligatoires");
print("<br><br><br>\n");
print("<form method=\"post\" action=\"main.php?form=4\">\n");
	

print " <font size=+1>
		<input type=\"radio\" name=\"desactive\" value=\"desactive\">
		désactiver les fonctions de vérifications de saisies
		</font>
		<br><br>";	

print("<table>\n");

print("<tr>\n");
printRow("tatouage *","text","tatouage","10");
printRow("poil","text","poil","5");
print("</tr>\n");

print("<tr>\n");
printRow("boucle *","text","boucle","10");
printRow("gras","text","gras","5");
print("</tr>\n");

print("<tr>\n");
printRow("naisseur <br>
			<font size=1>par défaut $ici</font>" , "text", "naisseur", "10");
printRow("persistance","text","persistance","5");
print("</tr>\n");

print " <tr>
		<td align=left width=\"90\">
		<label for=\"cornes\">
		<font size=5><b>
		cornes
		</b></font></label>
		</td>
		<td align=left width=\"100\">
		<font size=+2>
		<select name=\"cornes\">
		<option>n</option>
		<option selected>y</option></select>
		</td>";
printRow("caractère<br>
			<font size=1>15 caractères maxi</font>","text","caractère","15");
print("</tr>\n");

print " <tr>
		<td align=left width=\"90\">
		<label for=\"sexe\">
		<font size=5><b>
		sexe
		</b></font></label>
		</td>
		<td align=left width=\"100\">
		<font size=+2>
		<select name=\"sexe\">
		<option>m</option>
		<option selected>f</option></select>
		</td>
		</tr>";

print("<tr>\n");
printRow("date de sortie<br>
			<font size=3>format:jj-mm-aaaa<br></font>
			<font size=1>ou autre séparateur non numérique</font>",
			"date","sortie","10");
$option = array("mort","réforme");
printSelectedRow("cause sortie","cause_sortie",$option," ");
print("</tr>\n");

generateHtmlFoot("annuler", "insérer");

	if(!isset($boucle) && !isset($tatouage))
	{
		message("l'animal doit être identifié par boucle ET tatouage");
		exit();		
	}
	else
	{
		if(!verifSaisie("boucle",$tatouage))	
				verifValidite("^([0-9]{5})$","tatouage",$tatouage);
		if(!verifSaisie("boucle",$boucle))
				verifValidite("^[0-9]+$","boucle",$boucle);
		if($sortie)
				verifSaisie("cause de sortie",$cause_sortie);
		if($cause_sortie && !verifSaisie("date de sortie",$sortie))
				verifValidite("^([0-3][0-9])[^0-9]([0-1][0-9])[^0-9]([0-9]{4})$","date de sortie",$sortie);

		$note = array("poil"=>$poil, "gras"=>$gras, "persistance"=>$persistance);
		while(list($lbl,$field) = each($note))
		{
				if(! empty($field))
						verifValidite("^[\+-]{1,3}$", $lbl, $field);
		}
		if($caractère)
				verifLongueur("15", "caractère", $caractère);
		setNaisseur($naisseur, "naisseur", $mod_nelv);
		if($vérif && $desactive != "desactive")
				exit();

convertDate($sortie);
$sortie = $convdte;
$mysql_link = mysql_connect($db_server, $db_login, $db_password);
mysql_select_db($dbt, $mysql_link);

$q = "SELECT an_id FROM individus ";
$q .= "WHERE tatouage='$tatouage' AND boucle='$boucle' AND naisseur='$naisseur' ";
$r = mysql_query($q);
$m = mysql_affected_rows($mysql_link);
if($m)
{
		message("cet animal a déjà été enregistré");
		exit();
}

$query = "INSERT INTO individus ";
$query .= "(tatouage,boucle,cornes,sexe,poil,gras,persistance,caractère,sortie,cause_sortie,naisseur) ";
$query .= "VALUES ('$tatouage','$boucle','$cornes','$sexe','$poil','$gras','$persistance','$caractère','$sortie','$cause_sortie','$naisseur') ";
$mysql_result = mysql_query($query,$mysql_link);
maj($mysql_link);
	}
?>

<?php
		info("utiliser ce formulaire pour renseigner des animaux dont on ne connait pas l'ascendance.<br>
			si l'ascendance est connue, passer par 'mises_bas' et 'identification'.");
		print("<br><br><br>\n");
		détail("renseigner poil, gras, persistance avec des + ou des -, 3 au maximum");
		print("<br>\n");
		détail("les champs astérisqués sont obligatoires");
		print("<br><br><br>\n");
		print("<form method=\"post\" action=\"main.php?form=5\">\n");
	

print " <font size=+1>
		<input type=\"radio\" name=\"desactive\" value=\"desactive\">
		désactiver les fonctions de vérifications de saisies
		</font>
		<br><br>	

		<table>";

print("<tr>\n");
printRow("tatouage *","text","tatouage","10");
print " <td align=left width=\"120\">
		<label for=\"cornes\">
		<font size=5><b>
		cornes
		</b></font></label>
		</td>
		<td align=left width=\"100\">
		<font size=+2>
		<select name=\"cornes\">
		<option>n</option>
		<option selected>y</option></select>
		</td>";
printRow("poil","text","poil","5");
print("</tr>\n");

print("<tr>\n");
printRow("boucle *","text","boucle","10");
print " <td align=left width=\"90\">
		<label for=\"sexe\">
		<font size=5><b>
		sexe
		</b></font></label>
		</td>
		<td align=left width=\"100\">
		<font size=+2>
		<select name=\"sexe\">
		<option>m</option>
		<option selected>f</option></select>
		</td>";
printRow("gras","text","gras","5");
print("</tr>\n");

print("<tr>\n");
printRow("naisseur <br>
			<font size=1>par défaut $ici</font>" , "text", "naisseur_fille", "10");
print " <td align=left width=\"160\">
		<label for=\"cause_entrée\">
		<font size=5><b>
		cause d'entrée
		</b></font></label>
		</td>
		<td align=left width=\"100\">
		<font size=+2>
		<select name=\"cause_entrée\">
		<option>achat</option>
		<option selected>naissance</option></select>
		</td>";
printRow("persistance","text","persistance","5");
print("</tr>\n");

print("<tr>\n");
printRow("date de sortie<br>
			<font size=3>format:jj-mm-aaaa<br></font>
			<font size=1>ou autre séparateur non numérique</font>",
			"date","sortie","10");
$option = array("mort","réforme");
printSelectedRow("cause sortie","cause_sortie",$option," ");
printRow("caractère<br>
			<font size=1>15 caractères maxi</font>","text","caractère","15");
print("</tr>\n");

print("</table>\n");

print "	<p>
		<nobr>
		<font color=purple size=\"4\">
		Renseigner les champs suivants concernant la mère de l'animal précédent uniquement dans le cas d'un chargement initial de la base<br>
		sinon, passer par la mise-bas et l'identification.<br>
		Vous pouvez n'indiquer que la boucle, ou le tatouage, avec le champ 'naisseur' si il est différent de la valeur par défaut.
		</nobr>
		</p>";
		
print("<table>\n");
print("<tr>\n");
printRow("tatouage de la mère","text","tatouage_mère","10");
printRow("boucle de la mère","text","boucle_mère","10");
print("</tr>\n");

print("<tr>\n");
printRow("naisseur de  la mère<br>
			<font size=1>par défaut $ici</font>" , "text", "naisseur_mère", "10");
printRow("date de mise bas*<br>
			<font size=3>format:jj-mm-aaaa<br></font>
			<font size=1>ou autre séparateur non numérique</font>",
			"date","date_mb","10");
print("</tr>\n");

generateHtmlFoot("annuler", "insérer");

	if(!isset($tatouage) || !isset($boucle))
	{
		message("l'animal doit être identifié par boucle ET tatouage");
		exit();		
	}
	elseif(!isset($tatouage_mère) && !isset($boucle_mère))
	{
		message("la mère doit être identifiée par boucle ou tatouage");
		exit();				
	}
	else
	{
		if(!verifSaisie("boucle",$boucle))	
				verifValidite("^([0-9]{5})$","tatouage",$tatouage);
		if(!verifSaisie("boucle",$boucle))
				verifValidite("^[0-9]+$","boucle",$boucle);
		if($sortie)
				verifSaisie("cause de sortie",$cause_sortie);
		if($cause_sortie && !verifSaisie("date de sortie",$sortie))
				verifValidite("^([0-3][0-9])[^0-9]([0-1][0-9])[^0-9]([0-9]{4})$","date de sortie",$sortie);

		$note = array("poil"=>$poil, "gras"=>$gras, "persistance"=>$persistance);
		while(list($lbl,$field) = each($note))
		{
				if(! empty($field))
						verifValidite("^[\+-]{1,3}$", $lbl, $field);
		}
		if($caractère)
				verifLongueur("15", "caractère", $caractère);
		
		if(!isset($naisseur_fille))
		{
				$naisseur_fille = $ici;
		}
		else
		{
				verifValidite($mod_nelv, "naisseur", $naisseur_fille);
		}
		if(!isset($naisseur_mère))
		{
				$naisseur_mère = $ici;
		}
		else
		{
				verifValidite($mod_nelv, "naisseur de la mère", $naisseur_mère);
		}
		if(! verifSaisie("date",$date_mb))
			 	verifValidite($mod_dte,"date de mise bas",$date_mb);
		if($tatouage_mère)
				verifValidite($mod_ttg, "tatouage de la mère", $tatouage_mère);
		if($boucle_mère)
				verifValidite($mod_bcle, "boucle de la mère", $boucle_mère);
		if($vérif && $desactive != "desactive")
				exit();

				
convertDate($sortie);
$sortie = $convdte;
$mysql_link = mysql_connect($db_server, $db_login, $db_password);
mysql_select_db($dbt, $mysql_link);

//vérifie si on a pas déjà enregistré la fille

$q1 = "SELECT an_id FROM individus ";
$q1 .= "WHERE tatouage='$tatouage' AND boucle='$boucle' AND naisseur='$naisseur_fille' ";
$r1 = mysql_query($q1);
$m1 = mysql_affected_rows($mysql_link);
if($m1)
{
		message("cet animal (la fille) a déjà été enregistré");
		exit();
}

//vérifie et récupère an_id de la mère
existence($tatouage_mère, $boucle_mère, $naisseur_mère);

//insère la fille dans 'individus'
$query1 = "INSERT INTO individus ";
$query1 .= "(tatouage,boucle,cornes,sexe,poil,gras,persistance,caractère,sortie,cause_sortie,naisseur) ";
$query1 .= "VALUES ('$tatouage','$boucle','$cornes','$sexe','$poil','$gras','$persistance','$caractère','$sortie','$cause_sortie','$naisseur_fille') ";
$result1 = mysql_query($query1,$mysql_link);
$maj1 = mysql_affected_rows($mysql_link);
message("$maj1 mise à jour dans la table individus");
print("<br>\n");

convertDate($date_mb);
$date_mb = $convdte;


//vérifie l'existence d'une soeur et récupère l'id de la mise bas dans 'this_id'
$q2 = "SELECT id FROM mises_bas m, individus i ";
$q2 .= "WHERE m.an_id = i.an_id ";
$q2 .= "AND m.date_mb = '$date_mb' ";
$q2 .= "AND (i.tatouage = '$tatouage_mère' OR i.boucle = '$boucle_mère') ";
$q2 .= "AND i.naisseur = '$naisseur_mère' ";
$r2 = mysql_query($q2);
$m2 = mysql_affected_rows($mysql_link);
if($m2)
{
		//il y a une soeur
		//ne fait que rajouter 1 à 'nb_chevreaux'
		$this_id = mysql_result($r2, 0, "id");
		$query2 = "UPDATE mises_bas ";
		$query2 .= "SET nb_chevreaux =nb_chevreaux + 1 ";
		$query2 .= "WHERE id = '$this_id' ";
		$result2 = mysql_query($query2,$mysql_link);
		$maj2 = mysql_affected_rows($mysql_link);
		message("$maj2 mise à jour dans la table mises_bas");
		print("<br>\n");
}
else
{
	//si il n' y a pas de soeur	
	//insére la mise_bas	
	$query2 = "INSERT INTO mises_bas ";
	$query2 .= "(date_mb,an_id) ";
	$query2 .= "VALUES ('$date_mb', '$this_an_id') ";
	$result2 = mysql_query($query2,$mysql_link);
	$nid = mysql_insert_id($mysql_link);
	$maj2 = mysql_affected_rows($mysql_link);
	message("$maj2 mise à jour dans la table mises_bas");
	print("<br>\n");
}


if(!isset($m2))
{
		//pas de soeur, on va chercher l'id de la mise bas
		$query3 = "SELECT id ";
		$query3 .= "FROM mises_bas ";
		$query3 .= "ORDER BY id DESC LIMIT 1 ";
		$result3 = mysql_query($query3, $mysql_link);
		$my_id = mysql_result($result3, 0, "id");
}
else
{
		//il y avait une soeur
		$my_id = $this_id;
}


//récupère l'an_id de la fille
$query4 = "SELECT an_id from individus ";
$query4 .= "ORDER BY an_id DESC LIMIT 1 ";
$result4 = mysql_query($query4);
$an_id = mysql_result($result4, 0, "an_id");

//insère l'identification de la fille
$query5 = "INSERT INTO identification (id, entrée,cause_entrée,an_id) ";
$query5 .= "VALUES ('$my_id','$date_mb','$cause_entrée','$an_id')";
$result5 = mysql_query($query5);
$maj5 = mysql_affected_rows($mysql_link);
message("$maj5 mise à jour dans la table identification");
}
?>

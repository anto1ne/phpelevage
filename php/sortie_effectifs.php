<?php
include("../include/fonctions_tableaux.php");

if(!isset($type_requête))
{
	message("il faut choisir une option dans les cases à cocher");
	generateFormRequest("main.php?form=14");
	print " </body>
		</html>";
	exit;
}

$mysql_link = mysql_connect($db_server,$db_login,$db_password);
mysql_select_db($dbt, $mysql_link);

switch($type_requête)
	{
		case "animaux_J":

		if($date_début || $date_fin)
		{
			message("on n'utilise pas les champs de date dans cette option");
			exit();
		}
		$query = "SELECT tatouage,boucle,sexe ";
		$query .= "FROM individus ";
		$query .= "WHERE sortie='0000-00-00' ";
		$query .= "and (tatouage != '' AND boucle != '') ";
		$query .= "ORDER BY sexe";
		$mysql_result = mysql_query($query,$mysql_link);	

		$title = "animaux présents le ";
		$title .= date("d-m-Y");
		makeColouredTable($title, $mysql_result);

		break;
		
		case "effectif_J":
		
		if($date_début || $date_fin)
		{
			message("on n'utilise pas les champs de date dans cette option");
			exit();
		}
		$query = "SELECT sexe,count(an_id) AS effectif ";
		$query .= "FROM individus ";
		$query .= "WHERE sortie='0000-00-00' ";
		$query .= "and (tatouage != '' AND boucle != '') ";
		$query .= "GROUP BY sexe";
		$mysql_result = mysql_query($query,$mysql_link);

		$title = "effectif le ";
		$title .= date("d-m-Y");
		makeColouredTable($title, $mysql_result);
		break;


		case "variation":

		if(!verifSaisie("date de début de période", $date_début))
			verifValidite($mod_dte, "date de début de période", $date_début);
		if(!verifSaisie("date de fin de période", $date_fin))
			verifValidite($mod_dte, "date de fin de période", $date_fin);		
		if($vérif)
			exit();	
		if(verifPeriode($date_début, $date_fin))
			exit();
		

		$title = "variation d'effectif entre le " . $date_début
					. " et le " . $date_fin;

		convertDate($date_début);
		$date_début  = $convdte;
		convertDate($date_fin);
		$date_fin  = $convdte;
		
print "<table border=\"10\" cellspacing=\"6\">
	<tr>
	<thead>
	<tr bgcolor=9999cc>
	<td colspan=\"5\" align=center>
	<font size=\"5\"><b>$title</b></font>
	</td>
	</tr>
	</thead>
	<tr bgcolor=6699FF>
	<th> </th><th>femelles repro</th><th>males repro</th>
	<th>femelles renouvellement</th><th>males renouvellement</th></tr>";

//calcul de l'effectif début
/*
un renouvellement est un animal de moins de $renlimit jours
un adulte est un animal de plus de $renlimit jours
*/
print("<tr>\n");
print("<td bgcolor=6699FF align=center>effectif début</td>\n");
		function printEffectif($maj,$mysql_result)
		{
			switch($maj)
			{
				case "0":
				print("<td align=center>0</td><td align=center>0</td>");	
				break;
				
				case "2":
				while($row = mysql_fetch_row($mysql_result))
				{
					$effectif = $row[1];
					print("<td align=center>$effectif</td>");
				}
				break;				
				
				case "1":
				while($row = mysql_fetch_row($mysql_result))
				{
				$effectif[] = array($row[0],$row[1]);
				}
				$index = 0;
				if($effectif[$index][0] == "f")
				{
					print("<td align=center>");
					print($effectif[$index][1]);
					print("</td>");
					print("<td align=center>0</td>");
				}
				else
				{
					print("<td align=center>0</td>");
					print("<td align=center>");
					print($effectif[$index][1]);
					print("</td>");
				}
				break;
			}				
		}

		$query = "SELECT i1.sexe,count(i1.an_id)  ";
		$query .= "FROM individus i1,identification i2 ";
		$query .= "WHERE (i1.sortie >= '$date_début' OR i1.sortie='0') ";
		$query .= "AND i2.entrée < date_add('$date_début', interval -'$renlimit' day) ";
		$query .= "AND i1.an_id=i2.an_id ";
		$query .= "GROUP BY i1.sexe";
		$mysql_result = mysql_query($query,$mysql_link);
		$maj = mysql_affected_rows($mysql_link);
printEffectif($maj,$mysql_result);

		$query = "SELECT i1.sexe,count(i1.an_id) ";
		$query .= "FROM individus i1, identification i2 ";
		$query .= "WHERE (i1.sortie >= '$date_début' OR i1.sortie='0') ";
		$query .= "AND i2.entrée between date_add('$date_début', interval -'$renlimit' day) and '$date_début' ";
		$query .= "AND i1.an_id = i2.an_id ";
		$query .= "GROUP BY i1.sexe";
		$mysql_result = mysql_query($query,$mysql_link);
		$maj = mysql_affected_rows($mysql_link);
printEffectif($maj,$mysql_result);
print("</tr>\n");

//calcul des entrées achat
print("<tr>");
print("<td align=center bgcolor=6699FF>entrées achat</td>");
		$query = "SELECT i1.sexe,count(i1.an_id) ";
		$query .= "FROM individus i1, identification i2 ";
		$query .= "WHERE i2.tip_tag='' ";
		$query .= "AND i2.entrée BETWEEN '$date_début' AND '$date_fin' ";
		$query .= "AND i2.cause_entrée='achat' ";
		$query .= "AND i1.an_id = i2.an_id ";
		$query .= "GROUP BY i1.sexe";
		$mysql_result = mysql_query($query,$mysql_link);
		$maj = mysql_affected_rows($mysql_link);
printEffectif($maj,$mysql_result);

		$query = "SELECT i1.sexe,count(i1.an_id) ";
		$query .= "FROM individus i1, identification i2 ";		
		$query .= "WHERE i2.entrée BETWEEN '$date_début' AND '$date_fin' ";
		$query .= "AND i2.cause_entrée='achat' ";
		$query .= "AND i2.tip_tag != '' ";
		$query .= "AND i1.an_id = i2.an_id ";
		$query .= "GROUP BY i1.sexe";
		$mysql_result = mysql_query($query,$mysql_link);
		$maj = mysql_affected_rows($mysql_link);
printEffectif($maj,$mysql_result);
print("</tr>\n");

//calcul des entrées naissances
print("<tr>");
print("<td clign=center bgcolor=6699FF>entrées naissances</td>");
		$query = "SELECT i1.sexe,count(i1.an_id) ";
		$query .= "FROM individus i1, identification i2 ";
		$query .= "WHERE i2.tip_tag='' ";
		$query .= "AND i2.entrée BETWEEN '$date_début' AND '$date_fin' ";
		$query .= "AND i2.cause_entrée='naissance' ";
		$query .= "AND i1.an_id = i2.an_id ";
		$query .= "GROUP BY i1.sexe ";
		$mysql_result = mysql_query($query,$mysql_link);
		$maj = mysql_affected_rows($mysql_link);
printEffectif($maj,$mysql_result);

		$query = "SELECT i1.sexe,count(i1.an_id) ";
		$query .= "FROM individus i1, identification i2 ";
		$query .= "WHERE i2.entrée BETWEEN '$date_début' AND '$date_fin' ";
		$query .= "AND i2.tip_tag != '' ";
		$query .= "AND i2.cause_entrée='naissance' ";
		$query .= "AND i1.an_id = i2.an_id ";
		$query .= "GROUP BY i1.sexe";
		$mysql_result = mysql_query($query,$mysql_link);
		$maj = mysql_affected_rows($mysql_link);
printEffectif($maj,$mysql_result);
print("</tr>\n");

//calcul des sorties
print("<tr>");
print("<td align=center bgcolor=6699FF>sorties</td>");
		$query = "SELECT sexe,count(sortie) ";
		$query .= "FROM individus ";
		$query .= "WHERE sortie BETWEEN '$date_début' AND '$date_fin' ";
		$query .= "AND (tatouage != '' AND boucle != '') ";
		$query .= "GROUP BY sexe ";
		$mysql_result = mysql_query($query,$mysql_link);
		$maj = mysql_affected_rows($mysql_link);
printEffectif($maj,$mysql_result);

		$query = "SELECT sexe, count(an_id) ";
		$query .= "FROM individus ";
		$query .= "WHERE sortie BETWEEN '$date_début' AND '$date_fin' ";
		$query .= "AND (tatouage = '' OR boucle = '') ";
		$query .= "GROUP BY sexe ";
		$mysql_result = mysql_query($query, $mysql_link);
		$maj = mysql_affected_rows($mysql_link);
printEffectif($maj, $mysql_result);		
print("</tr>\n");

//calcul de l'effectif fin
print("<tr>");
print("<td align=center bgcolor=6699FF>effectif fin</td>");
		$query = "SELECT i1.sexe,count(i1.an_id) ";
		$query .= "FROM individus i1,identification i2 ";
		$query .= "WHERE (i1.sortie > '$date_fin' OR i1.sortie='0') ";
		$query .= "AND i2.entrée < date_add('$date_fin', interval -'$renlimit' day) ";
		$query .= "AND i1.an_id = i2.an_id ";
		$query .= "GROUP BY i1.sexe ";
		$mysql_result = mysql_query($query,$mysql_link);
		$maj = mysql_affected_rows($mysql_link);
printEffectif($maj,$mysql_result);

		$query = "SELECT i1.sexe,count(i1.an_id) ";
		$query .= "FROM individus i1, identification i2 ";
		$query .= "WHERE (i1.sortie = '0' OR i1.sortie > '$date_fin') ";
		$query .= "AND i2.entrée between date_add('$date_fin', interval -'$renlimit' day) and  '$date_fin' ";
		$query .= "AND i1.an_id = i2.an_id ";
		$query .= "GROUP BY i1.sexe ";
		$mysql_result = mysql_query($query,$mysql_link);
		$maj = mysql_affected_rows($mysql_link);
printEffectif($maj, $mysql_result);
print("</tr>\n");
	}
print("</table>\n");

generateFormRequest("main.php?form=14");

print " </body>
	</html>";
?>

<?php
include("../include/fonctions_tableaux.php");

$mysql_link = mysql_connect($db_server, $db_login, $db_password);
mysql_select_db($dbt, $mysql_link);

if(!isset($tatouage) && !isset($boucle))
{
	message("il faut identifier l'animal");
}
else
{
	setNaisseur($naisseur, "naisseur", $mod_nelv);	
	existence($tatouage, $boucle, $naisseur);
	
	$query1 = "SELECT i.tatouage, i.boucle FROM individus i ";
	$query1 .= "WHERE i.an_id = '$this_an_id' ";
	$result1 = mysql_query($query1, $mysql_link);
	$nb1 = mysql_affected_rows($mysql_link);
if(!isset($parentèle))
	$parentèle = "fiche";
	
	switch($parentèle)
	{
		case "ascendance":
			
		if($tatouage)
			verifValidite($mod_ttg, "tatouage", $tatouage);		
		if($boucle)
			verifValidite($mod_bcle, "boucle", $boucle);				
		if($vérif)
			exit();
			
		$query = "SELECT i.tatouage,i.boucle, ";
		$query .= "i.cornes,i.sexe,i.poil,i.gras,i.persistance, ";
		$query .= "i.caractère,i.sortie,i.cause_sortie ";
		$query .= "FROM individus i, mises_bas m, identification ii ";
	        $query .= "WHERE m.id = ii.id ";
		$query .= "AND i.an_id = m.an_id ";
		$query .= "AND ii.an_id = '$this_an_id' ";
			
		$title = "ascendance de:";
		
		break;

				
		case "descendance":
		
		if($tatouage)
			verifValidite($mod_ttg, "tatouage", $tatouage);		
		if($boucle)
			verifValidite($mod_bcle, "boucle", $boucle);				
		if($vérif)
			exit();
				
			
		$query = "SELECT i.tatouage,i.boucle, ";
		$query .= "i.cornes,i.sexe,i.poil,i.gras,i.persistance, ";
		$query .= "i.caractère,i.sortie,i.cause_sortie ";
		$query .= "FROM individus i,mises_bas m,identification ii ";
		$query .= "WHERE m.id=ii.id ";
		$query .= "AND m.an_id = '$this_an_id' ";
		$query .= "AND ii.an_id = i.an_id ";

		$title = "descendance de:";
			
		break;
		
		default:
		
		if($boucle)
			verifValidite($mod_bcle, "boucle", $boucle);
		if($tatouage)
			verifValidite($mod_ttg, "tatouage", $tatouage);
		if($vérif)
			exit();
					
		$query = "SELECT *  FROM individus i ";
		$query .= "WHERE i.an_id = '$this_an_id' ";

		$title = "fiche de:";
		break;

	}

$result2 = mysql_query($query, $mysql_link);
$nb = mysql_affected_rows($mysql_link);
$ttg = mysql_result($result1, 0, "tatouage");
$bcl = mysql_result($result1, 0, "boucle");
$title .= " tatouage= " . $ttg . ",boucle= " . $bcl;
if($parentèle == "ascendance" && $nb == 0)
{
	message("cet animal n'a pas de mère enregistrée");
	exit();
}
if($parentèle == "descendance" && $nb == 0)
{
	message("cet animal n'a pas de descendance");
	exit();
}
makeColouredTable($title,$result2);
}

generateFormRequest("main.php?form=13");
print "	</body>
	</html>";
		
?>

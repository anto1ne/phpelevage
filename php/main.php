<?php
include("../include/variables.php");
include("../include/fonctions_formulaires.php");
include("../include/fonctions_verifications.php");

$form = $_GET["form"];

switch($form)
{
	case 1:
	$nom = "formulaire mises-bas";
	$titre = "mises bas";
	$page ="mises_bas.php";
	break;

	case 2:
	$nom = "groupe mises-bas";
	$titre = "";
	$page = "groupe-mises_bas.php";
	$groupe = "oui";
	break;

	case 3:
	$nom = "formulaire identification";
	$titre = "identification";
	$page = "identification.php";
	break;

	case 4:
	$nom = "fiche individuelle";
	$titre = "individus";
	$page = "individus.php";
	break;	

	case 5:
	$nom = "individus plus";
	$titre = "individus plus";
	$page = "individus_plus.php";
	break;	
		
	case 6:
	$nom = "traitements individuels";
	$titre = "traitements individuels";
	$page = "traitements_individuels.php";
	break;	
	
	case 7:
	$nom = "groupe achat d'animaux";
	$titre = "";
	$page = "groupe-achat.php";
	$groupe = "oui";
	break;	

	case 7.1:
	$nom = "achat d'animaux";
	$titre = "achat d'animaux";
	$page = "achat_animaux.php";
	break;	
	
	case 7.2:
	$nom = "identification achat";
	$titre = "identification à l'achat";
	$page = "identification_achat.php";
	break;	

	case 8:
	$nom = "rebouclage";
	$titre = "rebouclage";
	$page = "rebouclage.php";
	break;

	case 9:
	$nom = "réforme adultes";
	$titre = "réforme d'adultes";
	$page = "reforme_adultes.php";
	break;

	case 10:
	$nom = "réforme renouvellement";
	$titre = "réforme de renouvellements";
	$page = "reforme_renouvellement.php";
	break;

	case 11:
	$nom = "identification définitive";
	$titre = "identification définitive";
	$page = "identification_def.php";
	break;
	
	case 12:
	$nom = "notation adultes";
	$titre = "notation des adultes";
	$page = "notation.php";
	break;

	case 13:
	$nom = "parentèle";
	$titre = "ascendance et descendance";
	$page = "parentele.php";
	break;

	case 13.1:
	$nom ="visualisation de parentèle";
	$titre = "ascendance et descendance";
	$page = "sortie_parentele.php";
	$sortie = "oui";
	break;
	
	case 14:
	$nom = "effectifs";
	$titre = "effectifs";
	$page = "effectifs.php";
	break;

	case 14.1:
	$nom = "visualisation des effectifs";
	$titre = "mes effectifs";
	$page = "sortie_effectifs.php";
	$sortie = "oui";
	break;

	case 15:
	$nom = "soins aux animaux";
	$titre = "soins";
	$page = "historique_soins.php";
	break;

	case 15.1:
	$nom = "historique des soins";
	$titre = "";
	$page = "sortie_soins.php";
	$sortie = "oui";
	break;

	case 16:
	$nom = "mes mises-bas";
	$titre = "mes mises-bas";
	$page = "historique_mb.php";
	break;

	case 16.1:
	$nom = "historique des mises-bas";
	$titre = "historique des mises-bas";
	$page = "sortie_mb.php";
	$sortie = "oui";
	break;

}
	
if(isset($sortie))
	$formcolor = "white";
if(!isset($groupe))
	generateHtmlHead($nom, $titre);

include($page);

?>


# PhpElevage


Phpelevage est une petite application écrite en php (version 4.0.5) qui utilise MySQL (version 3.23.36) avec un interfaçage en html servi par le serveur Apache (version 1.3.19).

Les formulaires d'insertion de données et de requêtes permettent le suivi d'un troupeau (de chèvres chez moi) :

  * enregistrement et suivi des mises bas.
  * enregistrement et suivi des filiations.
  * enregistrement et suivi des soins aux animaux.
  * attribution de 'notes' aux animaux pour garder en mémoires certaines caractéristiques individuelles.
  * enregistrement des mouvements d'animaux.
  * suivi des effectifs.

**Ces fichiers sont ceux initialement développés par [Jean-Michel Oltra](mailto:jm.oltra@club-internet.fr) et déposés [Linux-France.org](http://www.linux-france.org/ftp/article/cesar/) le 29 Octobre 2001.**

Version : 0.1  
Licence : GPLv2

Se référer au [Manuel](manuel.html) pour plus d'informations et la méthode d'installation.